//
//  Rest.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation

class Rest {
    
    class func loadMusics (term : String, onComplete: @escaping (Result) -> Void){
        
        guard let url = URL(string: "https://itunes.apple.com/search?term=\(term)&media=music") else {return}
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            if let data = data {
                
                do{
                    let musics = try JSONDecoder().decode(Result.self, from: data)
                    onComplete(musics)
                }catch {
                    print(error)
                }
            }
        }.resume()
    }
}
