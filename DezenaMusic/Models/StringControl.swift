//
//  StringControl.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation

class StringControl {
    
    class func separateMusicAndArtist(fullName: String) -> (String, String) {
        let newString = fullName.replacingOccurrences(of: "+", with: " ")
        let newArtistAndMusic = newString.components(separatedBy: "*")
        let nameMusic = newArtistAndMusic[0]
        var nameArtist = newArtistAndMusic[1]
        nameArtist.removeLast(4)
        return (nameMusic, nameArtist)
    }
}
