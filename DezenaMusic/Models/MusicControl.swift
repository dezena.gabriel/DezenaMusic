//
//  MusicControl.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class MusicControl {
    
    class func loadPlayer(urlMusic: String) -> AVPlayer {
        
        var player : AVPlayer
        let url = URL(string: urlMusic)
        let playerItem = AVPlayerItem(url: url!)
        player = AVPlayer(playerItem: playerItem)
        
        return player
    }
    
    class func loadPlayerOfDownloaded(urlMusic: URL) -> AVPlayer {
        
        var player : AVPlayer
        let playerItem = AVPlayerItem(url: urlMusic)
        player = AVPlayer(playerItem: playerItem)
        
        return player
    }
    
    class func playAndPause(player: AVPlayer, button: UIButton) {
        if player.rate == 0
        {
            button.setTitle("Pause", for: UIControlState.normal)
            player.play()
            
        } else {
            player.pause()
            button.setTitle("Play", for: UIControlState.normal)
        }
       
    }
    
        
}
