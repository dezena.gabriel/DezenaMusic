//
//  Protocols.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation

protocol PopUpDelegate {
    func showAlert(statusCode: Int)
}
