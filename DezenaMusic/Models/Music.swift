//
//  Music.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation

struct Result : Codable {
    
    var results : [Music]?
}

struct Music : Codable {
    
    var primaryGenreName : String
    var artistName : String
    var trackName : String
    var previewUrl : String
    var trackId : Int
    
}
