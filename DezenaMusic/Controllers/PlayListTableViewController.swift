//
//  PlayListTableViewController.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import UIKit
import AVFoundation

class PlayListTableViewController: UITableViewController {

    var term : String?
    var musics : [Music] = []
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = term
        Rest.loadMusics(term: term!) { (musics) in
            self.musics = musics.results!
            DispatchQueue.main.sync {
                self.tableView.reloadData()
            }
        }
        print(self.musics.count)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musics.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayListTableViewCell") as! PlayListTableViewCell
        cell.prepare(music: self.musics[indexPath.row])
        cell.delegatePop = self
        return cell
    }

}

extension PlayListTableViewController: PopUpDelegate{
    func showAlert(statusCode: Int) {
        if statusCode == 200{
            let alert = UIAlertController(title: "Success", message: "Music downloaded successfully", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
        }
    }

}
