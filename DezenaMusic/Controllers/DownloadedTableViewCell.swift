//
//  DownloadedTableViewCell.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import UIKit
import AVFoundation

class DownloadedTableViewCell: UITableViewCell {

    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lbNameMusic: UILabel!
    @IBOutlet weak var lbArtist: UILabel!
    var player : AVPlayer?
    var url : String?
    var fileName : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func playAndPause(_ sender: Any) {
        MusicControl.playAndPause(player: player!, button: btnPlay)
    }
    

}
