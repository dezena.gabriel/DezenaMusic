//
//  PlayListTableViewCell.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import UIKit
import AVFoundation

class PlayListTableViewCell: UITableViewCell {

    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lbNameMusic: UILabel!
    @IBOutlet weak var lbArtist: UILabel!
    var player : AVPlayer?
    var url : String?
    var fileName : String?
    var delegatePop: PopUpDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func download(_ sender: Any) {
        DownloadMusic.download(url: url!, fileName: fileName!) { (statusCode) in
            self.delegatePop?.showAlert(statusCode: statusCode)
        }

    }
    
    @IBAction func playAndPause(_ sender: Any) {
        MusicControl.playAndPause(player: player!, button: btnPlay)
    }
    
    func prepare(music: Music) {
        lbArtist.text = music.artistName
        lbNameMusic.text = music.trackName
        player = MusicControl.loadPlayer(urlMusic: music.previewUrl)
        url = music.previewUrl
        let newFileName = "\(music.trackName)*\(music.artistName)".replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
        fileName = "\(newFileName).m4a"
    }
}
