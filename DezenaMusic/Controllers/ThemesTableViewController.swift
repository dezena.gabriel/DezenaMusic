//
//  ThemesTableViewController.swift
//  DezenaMusic
//
//  Created by Gabriel Dezena on 28/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import UIKit

class ThemesTableViewController: UITableViewController {

    @IBOutlet weak var tfSearch: UITextField!
    var style : String?
    
    let styleList : [String] = ["indie","country","pop","rock","alternative","electronic"]
    var musics : [Music] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "THEMES"
        Rest.loadMusics(term: "indie") { (musics) in
            self.musics = musics.results!
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return styleList.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThemesTableViewCell") as! ThemesTableViewCell
        cell.lbTheme.text = styleList[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    @IBAction func Search(_ sender: Any) {
        let newString = tfSearch.text?.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
        self.style = newString
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Down" {
            
        }else{
            let vc = segue.destination as! PlayListTableViewController
            if self.style == nil{
                vc.term = styleList[(tableView.indexPathForSelectedRow?.row)!]
            }else{
                vc.term = self.style
            }
        }
    }
 

}
